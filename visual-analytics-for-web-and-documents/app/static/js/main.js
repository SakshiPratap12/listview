$(function(){
	addSamples();
	$(document).on('change', '.btn-file :file', function() {
		var input = $(this).parents('.input-group').find(':text');
		var label = $(this).val().replace(/\\/g, '/').replace(/.*\//, '');
		var extension=label.split(".").slice(-1);
		var isJson=extension=="json";
		var isCorrectFormat = extension=="tsv"||extension=="csv"||isJson
		
		if( isCorrectFormat ) {
			if(isJson){
				$("#hasHeaders").hide();
			}
			else{
				$("#hasHeaders").show();
			}

			input.val(label);
			$(".alert-warning").hide();
			$("#submitFile").prop("disabled",false);
		} else {
			$(".alert-warning").show();
			$("#submitFile").prop("disabled",true);
		}
	});
	
	$("#submitFile").on("click",function(e){
		var logEvent = new InteractionLog("click","upload-file","Submit button");
		interactionHistory.push(logEvent);
		e.preventDefault();
	    var formData = new FormData($("#fileUploadForm")[0]);
	    formData.append("headerPresent",$("#headercheckBox").is(':checked'));
	    $(".loader").removeClass("hidden");
	    $.ajax({
	    	url: '/listview/',
	        type : 'post',
	        data : formData,
	        async : true,
	        processData: false,  
	        contentType: false,  
	        }).done(function(data, textStatus, jqXHR){	
	              data=JSON.parse(data);
	        	  window.location.href = "/listview/"+data.url+"?id="+data.id;
	        }).fail(function(data){
	        	$(".loader").addClass("hidden");
	        	$(".alert-warning").show();
				$("#submitFile").prop("disabled",true);
	        });
		
	});
	
	function addSamples(){
		$.ajax({
			type: 'get',
			url: '/listview/getAllSessions',
			success: function(content){
				content=JSON.parse(content);
				var divs="";
				for (var i=0;i<content.length;i++){
					var current=content[i];
					divs+='<div class="col-md-4">'+
					 '<a href="/listview/session/'+current.name+'" target="_blank" data-name="'+current.name+'" class="sample-link no-decoration">'+
		             	 '<span class="sample-holder glyphicon list-glyphicon '+current.glyphicon+'"></span>'+
		             '</a>'+
		            '<div class="center"><small>'+current.displayName+'<a href="'+current.url+'" target="_blank"> <span class="font-sm glyphicon list-glyphicon glyphicon-link"></span></a></small></div>'+
		         '</div>';
					
				}
				$('.samples').append(divs);
			}
		})
		
		
	}
	
	$(document).on("click",".sample-link",function(){
		var logEvent = new InteractionLog("click","samples-"+$(this).attr("data-name"),"sampleimage");
		interactionHistory.push(logEvent);
	})
	
	var interactionHistory = new Array();

	var logEvent = new InteractionLog("connect","visualization","tool");
	interactionHistory.push(logEvent);

	// save interaction logs periodically
	setInterval(sendLog, 1000);

	function InteractionLog(manipulation,parameter,target,version){
	        this.timestamp = new Date().valueOf();
	        this.timezoneoffset = new Date().getTimezoneOffset();
	        this.manipulation = manipulation;
	        this.parameter = parameter;
	        this.target = target;
	        this.version = 2.1;
	        if(manipulation == "connect"){
                sessionStorage.setItem("starttimestamp", this.timestamp);
	        }
	        this.starttimestamp = sessionStorage.getItem("starttimestamp");
	}

	function sendLog(){
	        if(interactionHistory.length > 0){
	                $.ajax({
	                        url: "https://research.hsi.gatech.edu/logvis/logging/citevis_list_log.php",
	                        dataType: "jsonp",
	                        data: {interactionHistory: JSON.stringify(interactionHistory)},
	                        success: function(d){
	                                interactionHistory = [];
	                                console.log("Interactions successfully logged");
	                        }
	                });
	        }
	}
	

});