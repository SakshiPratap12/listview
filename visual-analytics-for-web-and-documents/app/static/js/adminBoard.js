$(function(){
	getSamples();

	
	function getSamples(){
		$.ajax({
			type: 'get',
			url: '/listview/getAllSessions',
			success: function(content){
				content=JSON.parse(content);
				var divs="";
				
				for (var i=0;i<content.length;i++){
					var current=content[i];
					divs+='<tr><td>'+current.name+'</td>'+
					'<td>'+current.displayName+'</td>'+
					'<td>'+current.url+'</td>'+
					'<td>'+current.session+'</td>'+
					'<td>'+current.glyphicon+'</td></tr>';
				}
				$('.samples').append(divs);
			}
		});
		
	}
	$(".submit-btn").click(function(){
		var temp={};
		$('input').each(function(){
			temp[$(this)[0].name]=$(this)[0].value
			
		});
		$.ajax({
			type:'post',
			contentType: 'application/json; charset=utf-8',
			url: '/listview/addSession',
			data: JSON.stringify(temp),
			success: function(content){
				window.location.reload();
			},
			error:function(){
				alert("error");
			}
		})
	 });
	
	$(".remove-submit-btn").click(function(){
		var temp={};
		temp['name']=$("#removeName")[0].value;
		$.ajax({
			type:'post',
			contentType: 'application/json; charset=utf-8',
			url: '/listview/removeSession',
			data: JSON.stringify(temp),
			success: function(content){
				alert("DOne");
			},
			error:function(){
				alert("error");
			}
		})
	 });
	
	

});