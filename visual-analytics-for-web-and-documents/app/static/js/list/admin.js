var sorter = {}
$(function(){
	var locationParam=location.search.split("&")[0].split("=");
	if(locationParam[0]=="?id")
		session_id=locationParam[1];
	else{
		window.location.href="/list"
	}
	$('.js-tooltip').tooltip();

	$('body').on('click','.js-add-splitter', function(){
		var $newRow = $('#row-template').clone();
		$newRow.attr("id","").removeClass('hidden').show();
		$('#split-rows').append($newRow);
		$('.js-tooltip').tooltip();
	});

	$('body').on('click', '.js-sub-splitter', function(){
		$(this).parents('.js-split-row:first').remove();
	});
	
	$.ajax({
		type: 'post',
		url: '/listview/rows/',
		contentType: 'application/json; charset=utf-8',
		data: JSON.stringify({'id': session_id}),
		success: function(content){
			$('#table-container').html(content);
		}
	});
	
	$("#visualizeButton").click(function(){
		window.location.href = '/listview/visualize'+"?id="+session_id;
	})

	$('#btnRename').click(function(){
		$("#renameError").addClass("hidden");
		var newColumnName = $('#newName').val();
		if(newColumnName && newColumnName !== $(this).data('column')){
			$('#btnRename').button('loading');
			$.ajax({
				type: 'post',
				contentType: 'application/json; charset=utf-8',
				url: '/listview/rename-column/page/'+ $('.js-pagination.js-active').text(),
				data: JSON.stringify({'old': $(this).data('column'), 'new': $('#newName').val(),'id': session_id}),
				success: function(content){
					if(JSON.parse(content).sucess==false)
						{
						$("#renameError").removeClass("hidden");
							return;
						}
						
					$('#table-container').html(JSON.parse(content).sucess);
					$('#newName').val("");
					$('#renameModal').modal('hide');
				}
			}).always(function () {
		      	$('#btnRename').button('reset');
		    });
		}
	});

	$('input[name="options"]').change(function(){
		switch($('input[name="options"]:checked').val()){
			case 'row':
				$('#split-rows').show();
				break;
			case 'col':
				$('#split-rows').hide();
				break;
		}
	});

	$('#btnSplit').click(function(){
		$('#btnSplit').button('loading');
		switch($('input[name="options"]:checked').val()){
			case 'row':
				splitIntoRows();
				break;
			case 'col':
				splitIntoCols();
				break;
		}
	});

	function splitIntoRows(){
		var json = {};
		json["keysToSplit"] = [$("#split-column-name").text()];
		json["separator"] = $('input[name="separator"]').val();
		json['id']= session_id

		$.ajax({
			type: 'post',
			contentType: 'application/json; charset=utf-8',
			url: '/listview/split-column-into-rows/page/'+ $('.js-pagination.js-active').text(),
			data: JSON.stringify(json),
			success: function(content){
				$('#table-container').html(content);
				$('#splitModal').modal('hide');
			}
		}).always(function () {
		    $('#btnSplit').button('reset');
		});
	}

	function splitIntoCols(){
		var json = {
			'column':$("#split-column-name").text(),
			'separator' : $('input[name="separator"]').val()
		};
		json['id']= session_id

		$.ajax({
			type: 'post',
			contentType: 'application/json; charset=utf-8',
			url: '/listview/split-column-into-cols/page/'+ $('.js-pagination.js-active').text(),
			data: JSON.stringify(json),
			success: function(content){
				$('#table-container').html(content);
				$('#splitModal').modal('hide');
			}
		}).always(function () {
		    $('#btnSplit').button('reset');
		});
	}
				

	$('body').on("click", ".js-menu-item", function(){
		var column = $(this).parents('div:first').find('.js-title').text(); 
		switch($(this).data('action')){
			case 'delete':
			$.ajax({
				type: 'post',
				contentType: 'application/json; charset=utf-8',
				url: '/listview/delete-column/page/'+ $('.js-pagination.js-active').text(),
				data: JSON.stringify({'column': column, 'id': session_id}),
				success: function(content){
					$('#table-container').html(content);
				}
			})
			break;
			case 'split':
				$('input[name="column[]"]').val(column);
				$('#split-column-name').text(column);
				$.ajax({
					type: 'post',
					url: '/listview/show-select',
					contentType: 'application/json; charset=utf-8',
					data: JSON.stringify({ 'id': session_id}),
					success: function(content){
						$('#column-select').empty().html(content);
						$('#splitModal').modal('show');
					}
				})
			
			break;
			case 'rename':
				$('#rename-column-name').text(column);
				$('#oldName').text(column);
				$('#btnRename').data('column', column);
				$('#renameModal').modal('show');
			break;
			case 'sort_asc':
			$.ajax({
				type:'post',
				contentType: 'application/json; charset=utf-8',
				url: '/listview/rows/sort/page/'+ $('.js-pagination.js-active').text(),
				data: JSON.stringify({'column': column, "sort": "asc",'id': session_id}),
				success: function(content){
					$('#table-container').html(content);
				}
			});
			break;
			case 'sort_desc':
			$.ajax({
				type:'post',
				contentType: 'application/json; charset=utf-8',
				url: '/listview/rows/sort/page/'+ $('.js-pagination.js-active').text(),
				data: JSON.stringify({'column': column, "sort": "desc",'id': session_id}),
				success: function(content){
					$('#table-container').html(content);
				}
			});
			break;
		}
		});

		$('body').on("click", ".js-pagination", function(){
			if(!$(this).hasClass('js-active')){
				var href = $(this).attr('href');
				var pagination_href = $(this).data('pagination-url');
				
				$.ajax({
					type: 'post',
					url: href,
					data: JSON.stringify({ 'id': session_id}),
					success: function(content){
						$('#table-container').html(content);
					}
				})
			
			}
			return false;
		});
});