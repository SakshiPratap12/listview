import sys
sys.path.insert(0, '/var/www/listView/listview/visual-analytics-for-web-and-documents')

from app import app as application

from flask.ext.triangle import Triangle

Triangle(application)

if __name__=="__main__":
        application.run(use_debugger = True)


